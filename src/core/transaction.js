import Controller from './controller'

/** 
 * Handling database features
 * @extends Controller
 */
class Transaction extends Controller {
  #global = null
  /**
   * Call to super statement
   * @param {string} location - Database location
   */
  constructor(location) {
    super(location)
  }

  /**
   * Add items to the database file
   * @param {string} name - The new item to add
   * @param {object} obj - The content of item to add
   * @returns {Object} - Pattern Chaining, return class
   */
  add(name, obj) {
    this.write(name, obj)
    return this
  }
  
  /**
   * Get the element
   * @param {string} name - The element to obtain
   * @param {object} [id] - The id of obj to find
   * @returns {object} - Get function or value
   */
  get(name, id=false) {
    const result = id ? this.findById(name, id) : this.find(name)

    this.#global = result

    return {
      get: this.get,
      // NOTE: After apply this
      // update: this.update.bind({
      //   name, 
      //   global: this.#global, 
      //   write: this.write
      // }),
      value: result
    }
  }

  /**
   * Update a value of one item
   * @param {string} name - Name of item to update
   * @param {object} value - New object to update
   */
  update(name, value) {
    super.update(name, value)
  }

  /**
   * 
   * @param {string} name - Name of item to delete
   * @param {object} [item=false] - Object of specific object to delete
   */
  delete(name, item=false) {
    super.remove(name, item)
  }
}

export default Transaction