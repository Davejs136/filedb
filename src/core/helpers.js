import { PATH } from './variables'
import { readJson, outputJson, remove, rmdir } from 'fs-extra'

// Short functionalities
export const isArray = Array.isArray

/**
 * Complete path to JSON file
 * @param {string} name - The JSON filename
 * @param {string} [location=''] - Location of the JSON file
 */
export function extract(name, location='') {
  const finalpath = (location === 'default' || location === '' 
  ? `${PATH}/${name}.json`
  : `${location}/${name}.json`)

  return finalpath
}

/**
 * Create the initial JSON file
 * @param {string} path - File path
 * @param {object} defaults - Default object content
 */
export async function createFile(path, defaults) {

  try {
    // Check if the location is default
    await readJson(path)
    console.info('Files exists')
    
  } catch (e) {
    // Create the files
    await outputJson(path, defaults)
    console.log('files created')
  }
}

/**
 * Delete JSON file 
 * @param {string} path - JSON file path
 */
export async function removeFile(path) {
  return await remove(path)
}

export async function removeFolder() {
  return await rmdir(PATH)
}