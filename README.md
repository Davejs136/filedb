# Filedb

<!-- [![](http://img.shields.io/npm/dm/lowdb.svg?style=flat)](https://www.npmjs.org/package/lowdb) [![Build Status](https://travis-ci.org/typicode/lowdb.svg?branch=master)](https://travis-ci.org/typicode/lowdb) [![npm](https://img.shields.io/npm/v/lowdb.svg)](https://www.npmjs.org/package/lowdb) -->

> Small and minimal JSON file database for Nodejs and Electron. :zap:

```js
db.add('posts', { 
  id: 1, 
  title: 'I like filedb'
})
```

## Usage

```sh
npm install filedb
```

```js
const FileDB = require('filedb')

const db = new FileDB({
  name: 'users', // File DB name
  localtion: 'default', // Custom or default
  // Set some defaults (Optional)
  defaults: {
    post: [],
    tasks: []
  }
})

// Add a new post
db.add('posts', { 
  id: 1, 
  title: 'I like filedb'
})

db.add('user', { name: 'Davejs' })

```

Data is saved to `yourNameDB.json`

```json
{
  "posts": [
    { "id": 1, "title": "I like filedb"}
  ],
  "user": {
    "name": "Davejs"
  }
}
```


Alternatively, if you're using [yarn](https://yarnpkg.com/)

```sh
yarn add filedb
```

<!-- # API

### Guide -->

### Examples

<!-- Check if posts exists. -->

<!-- ```js
db.has('posts')
  .value()
``` -->

Set data.

```js
db.add('posts', [])
```

Get posts.

```js
db.get('posts')
```


Update a post.

```js
db.update('posts', {
  id: 1,
  title: "Updated"
})
```

Remove posts.

```js
db.delete('posts')
```

Remove a single data post.

```js
db.delete('posts', { id: 1 })
```

## Changelog

Nothing yet.
<!-- See changes for each version in the [release notes](https://github.com/typicode/lowdb/releases). -->

## Limits

FileDB is a small database, that allow save data in JSON files. This way you can save data fast and quickly without to have a big database.

If you want have high performance and big scalability more than simplicity, then you should need use a traditional databases like MongoDB, CouchDB, etc.


## License

MIT - [Davejs :zap:](https://github.com/typicode)
